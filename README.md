# temp-convert

This simple application converts the temperature you give it between Celsius and Fahrenheit.

<details>
  <summary>Notes for GitHub users</summary>

This project has given up GitHub. ([See the Software Freedom Conservancy's *Give Up  GitHub* site for details](https://GiveUpGitHub.org).)

You can now find this project at [Codeberg](https://codeberg.org/osfanbuff63/temp-convert) instead.

Any use of this project's code by GitHub Copilot, past or present, is done without our permission. We do not consent to GitHub's use of this project's code in Copilot.

This GitHub repository will continue to exist as a read-only mirror - however issues and pull requests will **not** be accepted.

Join us; you can [give up GitHub](https://GiveUpGitHub.org) too!

![Logo of the GiveUpGitHub campaign](https://sfconservancy.org/img/GiveUpGitHub.png)

</details>

## Installation

**WARNING**: This application is still in beta! Bugs are expected!

<img alt="Download on Codeberg" height="40" src="https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/compact/available/codeberg_vector.svg">
<a href="https://pypi.org/project/temp-convert"><img alt="Download on PyPI" height="40" src="https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@2/assets/compact/available/pypi_vector.svg"></a>

You can install this using `pip`:

```bash
pip install temp-convert
```

## Usage

```bash
$ temp-convert --help
Usage: temp-convert [OPTIONS]

Options:
  -c, --celsius TEXT     Convert from Celsius to Fahrenheit.
  -f, --fahrenheit TEXT  Convert from Fahrenheit to Celsius.
  -v, --version          Show the version and exit.
  -h, --help             Show this message and exit.

# Examples
$ temp-convert -c 30
The temperature in Fahrenheit is 86.0°F.

$ temp-convert -f 32
The temperature in Celsius is 0.0°C.
```

## Contributing

Everyone is welcome to contribute to this project! This project is build using [`poetry`](https://python-poetry.org), so you'll need to [install it first](https://python-poetry.org/docs/#installation).

Clone the repository (`git clone https://codeberg.org/osfanbuff63/temp-convert`), then run `poetry install`. If you want to enter the virtual environment that Poetry creates, run `poetry shell` (recommended for larger changes) or prefix any commands with `poetry run`.

If you choose to contribute, you agree to license your contributions under the LGPLv3 (see below).

## License

This project is licensed under the GNU Lesser General Public License version 3 or, at your option, any later version. For the full license text, see [LICENSE.md](https://codeberg.org/osfanbuff63/temp-convert/src/branch/master/LICENSE.md) or [GNU.org](https://www.gnu.org/licenses/lgpl-3.0).
