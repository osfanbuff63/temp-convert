# temp-convert: A utility to convert the temperature.
# Copyright (C) 2022 osfanbuff63
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
import click
from click_pwsh import support_pwsh_shell_completion  # type: ignore

from temp_convert import __version__, calculate

support_pwsh_shell_completion()


@click.command(
    "temp-convert", context_settings=dict(help_option_names=["-h", "--help"])
)
@click.option(
    "--celsius",
    "-c",
    "celsius",
    help="Convert from Celsius to Fahrenheit.",
)
@click.option(
    "--fahrenheit",
    "-f",
    "fahrenheit",
    help="Convert from Fahrenheit to Celsius.",
)
@click.option(
    "--version", "-v", "version", help="Show the version and exit.", is_flag=True
)
def temp_convert(celsius, fahrenheit, version) -> None:
    if version:
        click.echo(f"temp-convert v{__version__}")
    elif celsius:
        converted_celsius = float(celsius)
        temperature = calculate.to_fahrenheit(converted_celsius)
        rounded_temperature = str(round(temperature, 1))
        click.echo(f"The temperature in Fahrenheit is {rounded_temperature}°F.")
    else:
        converted_fahrenheit = float(fahrenheit)
        temperature = calculate.to_celsius(converted_fahrenheit)
        rounded_temperature = str(round(temperature, 1))
        click.echo(f"The temperature in Celsius is {rounded_temperature}°C.")
