# temp-convert: A utility to convert the temperature.
# Copyright (C) 2022 osfanbuff63
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Calculate the conversions between temperature scales."""


def to_fahrenheit(celsius: float) -> float:
    """Calculate the conversion from Celsius to Fahrenheit.

    Args:
        celsius (float): The temperature in Celsius to convert.

    Returns:
        float: The temperature in Fahrenheit.
    """
    fahrenheit = float(celsius * 1.8 + 32)
    return fahrenheit


def to_celsius(fahrenheit: float) -> float:
    """Calculate the conversion from Fahrenheit to Celsius.

    Args:
        fahrenheit (float): The temperature in Fahrenheit to convert.

    Returns:
        float: The temperature in Celsius.
    """
    celsius = (fahrenheit - 32) * 5 / 9
    return celsius
